/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.trellisldp.ext.amanita;

import static java.util.Collections.emptyList;
import static javax.ws.rs.HttpMethod.GET;
import static javax.ws.rs.HttpMethod.HEAD;
import static javax.ws.rs.HttpMethod.OPTIONS;
import static javax.ws.rs.HttpMethod.POST;
import static javax.ws.rs.HttpMethod.PUT;
import static javax.ws.rs.Priorities.USER;
import static javax.ws.rs.core.HttpHeaders.CONTENT_TYPE;
import static javax.ws.rs.core.HttpHeaders.LINK;
import static javax.ws.rs.core.HttpHeaders.LOCATION;
import static javax.ws.rs.core.Link.TYPE;
import static javax.ws.rs.core.Link.fromUri;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.FOUND;
import static javax.ws.rs.core.Response.Status.Family.SUCCESSFUL;
import static javax.ws.rs.core.Response.status;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Priority;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

import org.trellisldp.vocabulary.LDP;
import org.trellisldp.vocabulary.Trellis;

@Provider
@Priority(USER)
public class ExternalContentFilter implements ContainerRequestFilter, ContainerResponseFilter {

    private static final String EXC_TYPE = "message";
    private static final String EXC_SUBTYPE = "external-body";
    private static final String EXC_HANDLING = "handling";
    private static final String EXC_REDIRECT = "redirect";
    private static final String EXC_PARAM_URL = "URL";
    private static final String EXC_PARAM_ACCESS_TYPE = "access-type";
    private static final String EXC_URI = "http://fedora.info/definitions/fcrepo#ExternalContent";

    @Override
    public void filter(final ContainerRequestContext req) throws IOException {
        if (POST.equals(req.getMethod()) || PUT.equals(req.getMethod())) {
            req.getHeaders().getOrDefault(LINK, emptyList()).stream().map(Link::valueOf)
                .filter(link -> EXC_URI.equals(link.getRel())).findFirst().ifPresent(link -> {
                    // we only support redirect
                    if (EXC_REDIRECT.equalsIgnoreCase(link.getParams().get(EXC_HANDLING))) {
                        final Map<String, String> params = new HashMap<>();
                        params.put(TYPE, link.getType());
                        params.put(EXC_PARAM_URL, link.getUri().toString());
                        params.put(EXC_PARAM_ACCESS_TYPE, EXC_PARAM_URL);
                        req.getHeaders().putSingle(LINK, fromUri(LDP.NonRDFSource.getIRIString()).rel(TYPE).build()
                                .toString());
                        req.getHeaders().putSingle(CONTENT_TYPE, new MediaType(EXC_TYPE, EXC_SUBTYPE, params)
                                .toString());
                    } else {
                        req.abortWith(status(BAD_REQUEST)
                                .link(Trellis.UnsupportedInteractionModel.getIRIString(),
                                    LDP.constrainedBy.getIRIString()).build());
                    }
                });
        }
    }

    @Override
    public void filter(final ContainerRequestContext req, final ContainerResponseContext res) throws IOException {
        if (res.getStatusInfo().getFamily().equals(SUCCESSFUL)) {
            if (req.getMethod().equals(OPTIONS)) {
                res.getHeaders().add("Accept-External-Content-Handling", "redirect");
            } else if ((GET.equals(req.getMethod()) || HEAD.equals(req.getMethod()))
                    && res.getLinks().stream().anyMatch(this::isNonRdfSource)) {
                final MediaType mt = res.getMediaType();
                if (mt.getType().equals(EXC_TYPE) && mt.getSubtype().equals(EXC_SUBTYPE)
                        && mt.getParameters().containsKey(TYPE)
                        && mt.getParameters().containsKey(EXC_PARAM_URL)) {
                    res.getHeaders().putSingle(CONTENT_TYPE, mt.getParameters().get(TYPE));
                    res.getHeaders().putSingle(LOCATION, mt.getParameters().get(EXC_PARAM_URL));
                    res.setEntity("");
                    res.setStatusInfo(FOUND);
                }
            }
        }
    }

    private Boolean isNonRdfSource(final Link link) {
        return link.getRel().equals(TYPE) && link.getUri().toString().equals(LDP.NonRDFSource.getIRIString());
    }
}
