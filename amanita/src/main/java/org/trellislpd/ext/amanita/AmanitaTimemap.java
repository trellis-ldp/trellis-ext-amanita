/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.trellisldp.ext.amanita;

import static java.util.Objects.nonNull;
import static java.util.stream.Stream.concat;
import static org.trellisldp.api.TrellisUtils.getInstance;
import static org.trellisldp.api.TrellisUtils.toGraph;
import static org.trellisldp.vocabulary.RDF.type;

import java.util.List;
import java.util.stream.Stream;

import javax.ws.rs.core.Link;

import org.apache.commons.rdf.api.Graph;
import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdf.api.RDF;
import org.apache.commons.rdf.api.Triple;
import org.trellisldp.http.core.TimemapGenerator;
import org.trellisldp.vocabulary.LDP;
import org.trellisldp.vocabulary.Memento;

public class AmanitaTimemap implements TimemapGenerator {

    private static final RDF rdf = getInstance();

    @Override
    public Stream<Triple> asRdf(final String identifier, final List<Link> mementos) {
        final Graph graph = TimemapGenerator.super.asRdf(identifier, mementos).collect(toGraph());

        final IRI timegate = graph.stream(null, Memento.timegate, null).map(Triple::getObject)
            .filter(IRI.class::isInstance).map(IRI.class::cast).findFirst().orElse(null);
        return concat(graph.stream(null, type, Memento.Memento).map(Triple::getSubject).filter(x -> nonNull(timegate))
                .map(memento -> rdf.createTriple(timegate, LDP.contains, memento)), graph.stream());
    }
}
