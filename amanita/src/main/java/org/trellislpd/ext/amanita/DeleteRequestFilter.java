/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.trellisldp.ext.amanita;

import static java.util.stream.Collectors.toList;
import static javax.ws.rs.HttpMethod.DELETE;
import static javax.ws.rs.Priorities.USER;
import static org.trellisldp.api.TrellisUtils.TRELLIS_DATA_PREFIX;
import static org.trellisldp.api.TrellisUtils.getInstance;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;

import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdf.api.RDF;
import org.apache.commons.rdf.api.Triple;
import org.trellisldp.api.Metadata;
import org.trellisldp.api.ResourceService;
import org.trellisldp.vocabulary.LDP;

@Provider
@Priority(USER)
public class DeleteRequestFilter implements ContainerRequestFilter {

    private static final RDF rdf = getInstance();

    private final ResourceService svc;

    /**
     * A recursive delete filter.
     * @param svc the resource service
     */
    @Inject
    public DeleteRequestFilter(final ResourceService svc) {
        this.svc = svc;
    }

    @Override
    public void filter(final ContainerRequestContext req) throws IOException {
        if (req.getMethod().equals(DELETE)) {
            final IRI identifier = rdf.createIRI(TRELLIS_DATA_PREFIX + req.getUriInfo().getPath());
            recursiveDelete(identifier);
        }
    }

    private void recursiveDelete(final IRI identifier) {
        final List<IRI> resources = svc.get(identifier).thenApply(res -> res.stream(LDP.PreferContainment)
                .map(Triple::getObject).filter(IRI.class::isInstance).map(IRI.class::cast).collect(toList())).join();
        resources.forEach(this::recursiveDelete);
        resources.stream().parallel()
            .map(id -> svc.delete(Metadata.builder(id).interactionModel(LDP.Resource).container(identifier).build()))
            .forEach(CompletableFuture::join);
    }
}
