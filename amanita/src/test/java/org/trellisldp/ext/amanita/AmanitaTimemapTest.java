/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.trellisldp.ext.amanita;

import static javax.ws.rs.core.Link.fromUri;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Link;

import org.apache.commons.rdf.api.Graph;
import org.apache.commons.rdf.api.RDF;
import org.apache.commons.rdf.jena.JenaRDF;
import org.junit.jupiter.api.Test;
import org.trellisldp.vocabulary.LDP;

public class AmanitaTimemapTest {

    private static final RDF rdf = new JenaRDF();
    private static final AmanitaTimemap timemap = new AmanitaTimemap();

    @Test
    public void testMementos() {
        final String identifier = "http://example.com/resource";
        final List<Link> mementos = new ArrayList<>();
        mementos.add(fromUri(identifier + "?version=1").rel("memento")
                .param("datetime", "Fri, 11 May 2018 15:29:25 GMT").build());
        mementos.add(fromUri(identifier + "?version=2").rel("memento")
                .param("datetime", "Sat, 12 May 2018 10:52:12 GMT").build());

        final Graph graph = rdf.createGraph();
        timemap.asRdf(identifier, mementos).forEach(graph::add);
        assertTrue(graph.stream(rdf.createIRI(identifier), LDP.contains,
                    rdf.createIRI(identifier + "?version=1")).findFirst().isPresent());
        assertTrue(graph.stream(rdf.createIRI(identifier), LDP.contains,
                    rdf.createIRI(identifier + "?version=2")).findFirst().isPresent());
    }
}
