/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.trellisldp.ext.amanita.app;

import static com.google.common.cache.CacheBuilder.newBuilder;
import static java.util.Objects.nonNull;
import static java.util.Optional.ofNullable;
import static java.util.concurrent.TimeUnit.HOURS;
import static javax.jms.Session.AUTO_ACKNOWLEDGE;
import static org.apache.jena.query.DatasetFactory.createTxnMem;
import static org.apache.jena.query.DatasetFactory.wrap;
import static org.apache.jena.rdfconnection.RDFConnectionFactory.connect;
import static org.apache.jena.tdb2.DatabaseMgr.connectDatasetGraph;
import static org.slf4j.LoggerFactory.getLogger;

import com.google.common.cache.Cache;

import io.dropwizard.lifecycle.AutoCloseableManager;
import io.dropwizard.setup.Environment;

import java.util.Optional;

import javax.jms.Connection;
import javax.jms.JMSException;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.jena.rdfconnection.RDFConnection;
import org.slf4j.Logger;
import org.trellisldp.agent.SimpleAgentService;
import org.trellisldp.api.AgentService;
import org.trellisldp.api.AuditService;
import org.trellisldp.api.BinaryService;
import org.trellisldp.api.DefaultIdentifierService;
import org.trellisldp.api.EventService;
import org.trellisldp.api.IOService;
import org.trellisldp.api.MementoService;
import org.trellisldp.api.NamespaceService;
import org.trellisldp.api.NoopEventService;
import org.trellisldp.api.RDFaWriterService;
import org.trellisldp.api.ResourceService;
import org.trellisldp.api.ServiceBundler;
import org.trellisldp.app.TrellisCache;
import org.trellisldp.app.config.NotificationsConfiguration;
import org.trellisldp.file.FileBinaryService;
import org.trellisldp.file.FileMementoService;
import org.trellisldp.io.JenaIOService;
import org.trellisldp.jms.JmsPublisher;
import org.trellisldp.namespaces.NamespacesJsonContext;
import org.trellisldp.rdfa.HtmlSerializer;
import org.trellisldp.triplestore.TriplestoreResourceService;

/**
 * A triplestore-based service bundler for Trellis.
 *
 * <p>This service bundler implementation is used with a Dropwizard-based application.
 * It combines a Triplestore-based resource service along with file-based binary and
 * memento storage. RDF processing is handled with Apache Jena.
 */
public class TrellisServiceBundler implements ServiceBundler {

    private static final String UN_KEY = "username";
    private static final String PW_KEY = "password";
    private static final Logger LOGGER = getLogger(TrellisServiceBundler.class);

    private final MementoService mementoService;
    private final AuditService auditService;
    private final TriplestoreResourceService resourceService;
    private final BinaryService binaryService;
    private final AgentService agentService;
    private final IOService ioService;
    private final EventService eventService;

    /**
     * Create a new application service bundler.
     * @param config the application configuration
     * @param environment the dropwizard environment
     */
    public TrellisServiceBundler(final AppConfiguration config, final Environment environment) {
        agentService = new SimpleAgentService();
        binaryService = new FileBinaryService(new DefaultIdentifierService(), config.getBinaries(),
                config.getBinaryHierarchyLevels(), config.getBinaryHierarchyLength());
        mementoService = new FileMementoService(config.getMementos());
        ioService = buildIoService(config);
        eventService = buildEventService(config.getNotifications(), environment);
        auditService = resourceService = buildResourceService(config, environment);
    }

    @Override
    public ResourceService getResourceService() {
        return resourceService;
    }

    @Override
    public IOService getIOService() {
        return ioService;
    }

    @Override
    public BinaryService getBinaryService() {
        return binaryService;
    }

    @Override
    public MementoService getMementoService() {
        return mementoService;
    }

    @Override
    public AuditService getAuditService() {
        return auditService;
    }

    @Override
    public AgentService getAgentService() {
        return agentService;
    }

    @Override
    public EventService getEventService() {
        return eventService;
    }

    private static TriplestoreResourceService buildResourceService(final AppConfiguration config,
            final Environment environment) {
        final RDFConnection rdfConnection = getRDFConnection(config);

        // Health checks
        environment.healthChecks().register("rdfconnection", new RDFConnectionHealthCheck(rdfConnection));
        final TriplestoreResourceService svc = new TriplestoreResourceService(rdfConnection);
        svc.initialize();
        return svc;
    }

    private static RDFConnection getRDFConnection(final AppConfiguration config) {
        final Optional<String> location = ofNullable(config.getResources());
        if (location.isPresent()) {
            final String loc = location.get();
            if (loc.startsWith("http://") || loc.startsWith("https://")) {
                // Remote
                return connect(loc);
            }
            // TDB2
            return connect(wrap(connectDatasetGraph(loc)));
        }
        // in-memory
        return connect(createTxnMem());
    }

    private static IOService buildIoService(final AppConfiguration config) {
        final Long cacheSize = config.getJsonld().getCacheSize();
        final Long hours = config.getJsonld().getCacheExpireHours();
        final Cache<String, String> cache = newBuilder().maximumSize(cacheSize).expireAfterAccess(hours, HOURS).build();
        final TrellisCache<String, String> profileCache = new TrellisCache<>(cache);
        final NamespaceService namespaceService = new NamespacesJsonContext(config.getNamespaces());
        final RDFaWriterService htmlSerializer = new HtmlSerializer(namespaceService, config.getAssets().getTemplate(),
                config.getAssets().getCss(), config.getAssets().getJs(), config.getAssets().getIcon());
        return new JenaIOService(namespaceService, htmlSerializer, profileCache,
                config.getJsonld().getContextWhitelist(), config.getJsonld().getContextDomainWhitelist());
    }

    private static EventService buildEventService(final NotificationsConfiguration config,
            final Environment environment) {
        if (config.getEnabled() && nonNull(config.getConnectionString())) {
            LOGGER.info("Connecting to JMS broker at {}", config.getConnectionString());
            try {
                final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(
                        config.getConnectionString());
                if (config.any().containsKey(PW_KEY) && config.any().containsKey(UN_KEY)) {
                    factory.setUserName(config.any().get(UN_KEY));
                    factory.setPassword(config.any().get(PW_KEY));
                }

                final Connection jmsConnection = factory.createConnection();
                environment.lifecycle().manage(new AutoCloseableManager(jmsConnection));
                return new JmsPublisher(jmsConnection.createSession(false, AUTO_ACKNOWLEDGE), config.getTopicName());
            } catch (final JMSException ex) {
                LOGGER.warn("Error connecting to JMS broker: {}", ex.getMessage());
            }
        }
        LOGGER.info("Using no-op event service");
        return new NoopEventService();
    }
}


